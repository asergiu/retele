import threading
import multiprocessing
import timeit


# depending on how large the x is multiprocessing might be faster
def countdown():
     x = 100000000
     while x > 0:
           x -= 1
# Implementation 1: Multi-threading

def implementation_1():
     thread_1 = threading.Thread(target=countdown)
     thread_2 = threading.Thread(target=countdown)
     thread_1.start()
     thread_2.start()
     thread_1.join()
     thread_2.join()

# Implementation 2: Run in serial
def implementation_2():
     countdown()
     countdown()


def implementation_3():
    process_1 = multiprocessing.Process(target=countdown)
    process_2 = multiprocessing.Process(target=countdown)
    process_1.start()
    process_2.start()
    process_1.join()
    process_2.join()



if __name__=='__main__':
    print(timeit.timeit('implementation_1()', globals=globals(), number=1))
    print(timeit.timeit('implementation_2()', globals=globals(), number=1))
    print(timeit.timeit('implementation_3()', globals=globals(), number=1))