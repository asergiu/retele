import ctypes
import sys
import time
from multiprocessing import Process, Manager

def f(d):
    # sys.stdin = open(0)
    # d[('1.1.1.1', 40)] = 1
    # d[('2.2.2.2', 140)] = 1
    # print(d)
    time.sleep(1)
    for key in d.keys():
        print(key)


if __name__ == '__main__':
    manager = Manager()
    d = manager.dict()

    p = Process(target=f, args=(d,))
    p.start()
    d[('3.3.3.3', 3000)] = 1
    d[('1.1.1.1', 40)] = 1
    d[('2.2.2.2', 140)] = 1


    # p.terminate()
    p.join()

    # for key in d:
    #     print(key)
