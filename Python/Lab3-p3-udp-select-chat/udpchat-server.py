
import socket, struct, sys, select
import string

clients = {} #(tcp_socket: (ip, udp_port))
rlist=[]

def remove_client(tcp_socket):
    removed_count=0
    to_remove = notify_others(b"D", tcp_socket, clients[tcp_socket])
    if tcp_socket in clients:
        del clients[tcp_socket]
        removed_count += 1
    if tcp_socket in rlist:
        rlist.remove(tcp_socket)
    tcp_socket.close()
    #recursively remove clients with errors
    for c in to_remove:
        removed_count = removed_count + remove_client(c)
    return removed_count


#return a list of other clients that cannot be updated => their TCP connection is closed
def notify_others(operation, tcp_socket, udp_addr) -> []:
    pending_error_clients=[]
    for c in clients:
        if c!=tcp_socket:
            try:
                c.send(operation)
                c.send(socket.inet_aton(udp_addr[0]))
                c.send(struct.pack("!H",udp_addr[1]))
            except OSError as e:
                pending_error_clients.append(c)
                continue
    return pending_error_clients

if __name__ == '__main__':
    try:
        rdv_socket = socket.create_server(address=('', int(sys.argv[1])),family=socket.AF_INET,backlog=5)
    except OSError as e:
        print(e.strerror)
        exit(-1)

    rlist = [rdv_socket]
    while True:
        toread,_,_ = select.select(rlist,[],[])
        for sock_read in toread:
            if rdv_socket==sock_read:
                try:
                    client_sock,client_addr = rdv_socket.accept()
                    #read client's UDP port
                    client_port = struct.unpack("!H",client_sock.recv(2))[0]
                    print("New client {}:{}".format(client_addr[0],client_port))

                    #send all other clients list to the new client
                    #send the length of the list first as 4byte uint
                    client_sock.send(struct.pack("!I",len(clients)))
                    for csock in clients:
                        client_sock.send(socket.inet_aton(clients[csock][0]))
                        client_sock.send(struct.pack("!H", clients[csock][1]))
                except  OSError as e:
                    print("Error adding client {}:{}".format(clients.addr[0],client_port),"  --- ",e.strerror)
                    #ignore this clent if we cannot send it the list of other clients
                    continue

                # send this client to all other clients
                to_remove = notify_others(b"N",client_sock, (client_addr[0],client_port))
                #if any other client is pending errors remove it
                for c in to_remove:
                    remove_client(c)

                #add the new client to rlist and clients
                rlist.append(client_sock)
                clients[client_sock] = (client_addr[0],client_port)
            else:
                try:
                    # we get here a command from a client (disconnect at this time)
                    operation=sock_read.recv(1)
                except OSError as e:
                    print("Client {}:{} has closed connection".format(clients[sock_read][0],clients[sock_read][1],"---",e.strerror))
                    remove_client(sock_read)
                    continue

                if operation == b"D" or operation == b'':
                    departing_client = clients[sock_read]
                    remove_client(sock_read)
                    print("Client {}:{} has quit!".format(departing_client[0], departing_client[1]))