import ctypes
import socket, struct, sys, select
import threading
# import multiprocessing

# manager = multiprocessing.Manager()
# looping = manager.Value(ctypes.c_bool,True)
looping = True
# clients = manager.dict()
clients = dict()
rlist=[]
my_udp_addr=('0.0.0.0',0)
lock = threading.Lock()



def keyboard_thread(udp_s):
    global lock, clients, looping
    # sys.stdin = open(0)
    my_udp_addr = udp_s.getsockname()
    print(my_udp_addr, ":", end='')
    msg = input()
    while msg != "q":
        lock.acquire()
        for peer in clients.keys():
            udp_s.sendto(bytes(msg.encode()), peer)
        lock.release()
        print(my_udp_addr, ":", end='')
        msg = input()
    looping = False


if __name__ == "__main__":
    try:
        tcp_sock=socket.create_connection( (sys.argv[1],int(sys.argv[2])) )
        udp_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        udp_sock.sendto(b"aaa", ("8.8.8.8", 53))
        my_udp_addr = udp_sock.getsockname()
        print("My endoint is: {}:{}".format(my_udp_addr[0], my_udp_addr[1]))
        tcp_sock.send(struct.pack("!H",my_udp_addr[1]))
    except OSError as e:
        print(e.strerror)
        exit(-1)


    #read the clients list len as 4 byte integer
    try:
        clients_len = struct.unpack("!I",tcp_sock.recv(4))[0]
        for i in range(clients_len):
            remote_ip = socket.inet_ntoa( tcp_sock.recv(4) )
            remote_udp_port = struct.unpack("!H", tcp_sock.recv(2))[0]
            lock.acquire()
            clients[(remote_ip, remote_udp_port)]=1
            lock.release()
    except OSError as e:
        print("Error receiving clients: ",e.strerror)
        exit(-1)

    #create a process to read from keyboard. select on standard input does not work on windows
    thread = threading.Thread(target=keyboard_thread,args=(udp_sock,), daemon=True)
    # thread = multiprocessing.Process(target=keyboard_thread, args=(udp_sock,clients,looping))
    thread.start()

    rlist = [tcp_sock,udp_sock]

    while looping:
        to_read,_,_ = select.select(rlist,[],[],.5)
        if tcp_sock in to_read:
            try:
                operation=tcp_sock.recv(1)
                peer_ip = socket.inet_ntoa( tcp_sock.recv(4) )
                peer_port = struct.unpack("!H",tcp_sock.recv(2))[0]

                lock.acquire()
                if operation == b"N":
                    clients[(peer_ip, peer_port)]=1
                elif operation == b"D":
                    del clients[(peer_ip,peer_port)]
                    print("\n"+peer_ip+":"+str(peer_port),"has left...")
                lock.release()
            except OSError as e:
                print(e.strerror,"- Server bailed out - quitting...")
                break

        if udp_sock in to_read:
            msg, peer_addr = udp_sock.recvfrom(65000)
            print("\n"+peer_addr[0]+":"+str(peer_addr[1]), msg.decode())

    print('Cleaning up...')
    try:
        tcp_sock.send(b"D")
        tcp_sock.close()
        udp_sock.close()
    except OSError:
        pass
    # if thread.is_alive():
    #     thread.terminate()
    thread.join(1)
    # manager.shutdown()