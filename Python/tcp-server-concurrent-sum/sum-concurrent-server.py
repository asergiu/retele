import socket
import multiprocessing as mp
import struct
import sys
import ctypes

def handle_client(sock):
    try:
        print("Client connected from: "+ str(sock.getpeername()))
        a = struct.unpack("!H",sock.recv(ctypes.sizeof(ctypes.c_ushort)))
        a= a[0]
        b = struct.unpack("!H",sock.recv(ctypes.sizeof(ctypes.c_ushort)))
        b = b[0]
        c = a+b
        len = sock.send(struct.pack("!H", c))
        if len < ctypes.sizeof(ctypes.c_ushort):
            print("to handle this situation")
    except OSError as err:
        print(err.strerror)
    return 0;

if __name__=='__main__':
    # rdv socket
    try:
        rdv = socket.create_server(("0.0.0.0",1234),family=socket.AF_INET, backlog=5 )
        print("Sum server waiting on port 1234")
        while True:
           cs,addr = rdv.accept()
           mp.Process(target=handle_client, args=(cs,)).start()
    except OSError as err:
        print("Socket error: "+err.strerror)



