__author__ = 'dadi'

import sys, socket

if __name__ == '__main__':
    try:
        s = socket.create_connection( ('localhost',1234))
    except socket.error as msg:
        print("Error: ",msg.strerror)
        exit(-1)
    if s is None:
        print("Cannot connect to server")
        exit(0)

    try:
#        data=s.recv(2000)

        s.send(bytes(sys.argv[1],'ascii'))
        while True:
            data=s.recv(30000)
            if len(data)==0:
                break
            print(data.decode('ascii', errors='ignore'))
        s.close()
    except OSError as msg:
        print("OSError,",msg.strerror)

