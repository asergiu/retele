from threading import Thread
from queue import Queue
import time
import random

myq = Queue()

def ttt(name):
    while True:
        item = myq.get()
        print(name + item + "  ---queue length: " + str(myq.qsize()) )
        myq.task_done()
        if myq.qsize() < 10:
            time.sleep(random.uniform(0, 1))
    return

if __name__ == "__main__":
    t1 = Thread(target=ttt, args=("T1: ",), daemon=True)
    t2 = Thread(target=ttt, args=("T2: ",), daemon=True)
    t1.start()
    t2.start()
    while True:
        if myq.qsize() > 5 :
            time.sleep(random.uniform(0,1))
        a = random.randrange(ord('A'),ord('Z'),1)
        # print(chr(a))
        myq.put(chr(a))