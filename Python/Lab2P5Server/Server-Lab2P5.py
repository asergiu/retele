__author__ = 'dadi'

import socket

if __name__=='__main__':
    try:
        rs=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        rs.bind( ('',1234) )
        rs.listen(100)
    except socket.error as msg:
        print(msg.strerror)
        exit(-1)

    while True:
        try:
            client_socket,addrc = rs.accept()
            host = client_socket.recv(1024)
            ns = socket.create_connection((host,80))
        except OSError as msg:
            print("Error socket", msg.strerror)
            client_socket.send(b'Error connecting to your site: '+host)
            client_socket.close()
            continue

        try:
            ns.send(b'GET / HTTP/1.0\r\n\r\n')
            stopsrv = False
            while not stopsrv:
                data = ns.recv(65000)
                print("Read",len(data))
                if len(data)==0:
                    stopsrv=True
                client_socket.sendall(data)
        except OSError as msg:
            print("OSerror - something bad happened", msg.strerror)
        ns.close()
        client_socket.close()



