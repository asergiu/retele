
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h>
#include <sys/types.h> 
#include <signal.h>
#include <iostream>
#include <ws2tcpip.h>
#include <winsock2.h>

using namespace std;

bool lsn = true;

void manage_signal(int code)
{
    lsn = false;
}

int main(int argc, char **argv)
{
    char data[1024];
    char ipasstring[20];
    int size = 0;
    int len = sizeof(sockaddr_in);
    int port = atoi(argv[1]);
    
    //   signal(SIGINT, manage_signal);

    WSADATA wsa;
    if (WSAStartup(MAKEWORD(2, 2), &wsa))
    {
        printf("Failed to initialize winsock. Error Code : %d", WSAGetLastError());
        return 1;
    }

    SOCKET sfd = socket(AF_INET, SOCK_DGRAM, 0);
       
    sockaddr_in listen;
    listen.sin_addr.s_addr = INADDR_ANY; // inet_addr("0.0.0.0") = 0
    listen.sin_port = htons(atoi(argv[1]));
    listen.sin_family = AF_INET;
    if (bind(sfd, (sockaddr*)&listen, len) < 0)
    {
        cerr << "Error binding socket: " << endl;
        return EXIT_FAILURE;
    }
    cout << "Listening for UDP packets on port " << port << endl;
    unsigned short int trueflag = 1;
    if (setsockopt(sfd, SOL_SOCKET, SO_BROADCAST, (char*)&trueflag, sizeof(trueflag)) == SOCKET_ERROR) {
        cerr << "cannot set socket to broadcast: " <<  endl;
        return EXIT_FAILURE;
    }

    while (lsn){
        sockaddr_in rcv;
        memset(&rcv,0,sizeof(rcv));
        int rlen = sizeof(rcv);
        size = recvfrom(sfd, data, sizeof(data), 0, (sockaddr*)&rcv, &rlen);
        data[size] = 0;
        //cout << data << " " << inet_ntoa(rcv.sin_addr) << endl;
        cout << "Received from "<< inet_ntop(AF_INET,&(rcv.sin_addr),ipasstring,sizeof(ipasstring))  <<":"<<ntohs(rcv.sin_port) << ": " << data << endl;
    }
    closesocket(sfd);
    WSACleanup();
    return 0;
}


