// broadcast_sender.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <stdio.h>      /* for printf() and fprintf() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <winsock2.h>
#include <ws2tcpip.h>
#include <synchapi.h>

int main(int argc, char* argv[])
{
    SOCKET sock;                         /* Socket */
    struct sockaddr_in broadcastAddr; /* Broadcast address */
    char* broadcastIP;                /* IP broadcast address */
    unsigned short broadcastPort;     /* Server port */
    char* sendString;                 /* String to broadcast */
    char  broadcastPermission;          /* Socket opt to set permission to broadcast */
    unsigned int sendStringLen;       /* Length of string to broadcast */
    int err;

    if (argc < 4)                     /* Test for correct number of parameters */
    {
        fprintf(stderr, "Usage:  %s <IP Address> <Port> <Send String>\n", argv[0]);
        exit(1);
    }

    WSADATA wsa;
    if (WSAStartup(MAKEWORD(2, 2), &wsa))
    {
        printf("Failed to initialize winsock. Error Code : %d", WSAGetLastError());
        return 1;
    }

    broadcastIP = argv[1];            /* First arg:  broadcast IP address */
    broadcastPort = atoi(argv[2]);    /* Second arg:  broadcast port */
    sendString = argv[3];             /* Third arg:  string to broadcast */

    /* Create socket for sending/receiving datagrams */
    if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
        perror("socket() failed");

    /* Set socket to allow broadcast */
    broadcastPermission = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &broadcastPermission,
        sizeof(broadcastPermission)) < 0)
        perror("setsockopt() failed");

    /* Construct local address structure */
    memset(&broadcastAddr, 0, sizeof(broadcastAddr));   /* Zero out structure */
    broadcastAddr.sin_family = AF_INET;                 /* Internet address family */
    /* Broadcast IP address */
    if ( (err = inet_pton(AF_INET, broadcastIP, &(broadcastAddr.sin_addr.s_addr))) != 1 )
        perror("Error ip addresss:");
    broadcastAddr.sin_port = htons(broadcastPort);         /* Broadcast port */

    sendStringLen = strlen(sendString);  /* Find length of sendString */
    for (;;) /* Run forever */
    {
        int nBytes = 0;
        /* Broadcast sendString in datagram to clients every 3 seconds*/
        if ( (nBytes = sendto(sock, sendString, sendStringLen, 0, (struct sockaddr*)&broadcastAddr, sizeof(broadcastAddr))) != sendStringLen)
            perror("sendto() sent a different number of bytes than expected");

        printf("Sent %s to %s:%d\n", sendString, broadcastIP, broadcastPort);
        Sleep(3000);   /* Avoids flooding the network */
    }
    /* NOT REACHED */
    WSACleanup();
}

