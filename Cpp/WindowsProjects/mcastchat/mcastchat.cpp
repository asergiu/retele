// mcastchat.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Winsock2.h"
#include "Ws2tcpip.h"
#include "process.h"


typedef struct HEADER {
	char ver[8];
	char room[32];
	char id[32];
	char oper;
	short msglen;
} header;

struct CHATMSG {
	struct HEADER hdr;
	char text[1024];
} mesg_in, mesg_out;

struct ip_mreq mreq;
SOCKET s;
struct sockaddr_in remote;
struct sockaddr_in mcast;
int remote_len;
fd_set set,master;
char stdin_msg[1024];
bool quit = false;


void readline_send(void *dummy ){
	while(!quit) {
		memset(mesg_out.text, 0, sizeof(mesg_out.text));
		gets_s(mesg_out.text, sizeof(mesg_out.text)-1);
		if ( strncmp(mesg_out.text,"QUIT", 4)== 0 ) {
			quit = true;
			return;
		}
		mesg_out.hdr.oper = 3;
		mesg_out.hdr.msglen = strlen(mesg_out.text)+1;
		if ( sendto(s, (char *)&mesg_out, sizeof(header)+mesg_out.hdr.msglen, 0,(struct sockaddr*)&mcast, sizeof(mcast)) < 0 )
			printf("Error sendto\n");
	}
}

int main(int argc, char* argv[])
{
	WORD vers = MAKEWORD(2,2);
	WSADATA wsa;
	struct sockaddr_in local;

	printf("%s\n", "");
	if (  WSAStartup(vers, &wsa) < 0  ){ 
		printf("Error initializing Winsock\n");
		return -1;
	}
	printf("Using winsock version %d.%d\n", LOBYTE(wsa.wVersion), HIBYTE(wsa.wVersion) );

	s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (s==INVALID_SOCKET) {
		printf("Error constructing the socket %d\n", WSAGetLastError());
		return -1;
	}
	
	local.sin_family = AF_INET;
	local.sin_addr.S_un.S_addr = inet_addr(INADDR_ANY);
	local.sin_port = htons(7777);
	if ( bind(s, (struct sockaddr*)&local, sizeof(local))< 0 ){
		printf("bind error %d\n", WSAGetLastError());
		return -1;
	}

	mcast.sin_family=AF_INET;
	mcast.sin_port = htons(7777);
	mcast.sin_addr.S_un.S_addr = inet_addr(argv[1]);

	mreq.imr_multiaddr.S_un.S_addr = inet_addr(argv[1]);
	mreq.imr_interface.S_un.S_addr = 0;
	if (setsockopt(s,IPPROTO_IP, IP_ADD_MEMBERSHIP, (char*)&mreq, sizeof(mreq)) <0){
		printf("Error add membership %d\n", WSAGetLastError());
		return -1;
	}

	memset(&remote, 0, sizeof(remote));
	int res=0;
	memset(&mesg_out, 0, sizeof(mesg_out));
	strcpy(mesg_out.hdr.ver, "CHATv1");
	strcpy(mesg_out.hdr.id, "dadi");
	strcpy(mesg_out.hdr.room, "#cluj");
	fflush(stdout);
	_beginthread(readline_send, 2000,NULL);

	while (!quit) {
		remote_len = sizeof(remote);
		res = recvfrom(s, (char*)&mesg_in, sizeof(mesg_in), 0, (struct sockaddr*)&remote, &remote_len);
		if (res < 0 ) {
			printf("Error recvfrom %d\n", WSAGetLastError());
		}
		printf("Msg from %s -- NICK %s ROOM %s oper %c -- %s\n", inet_ntoa(remote.sin_addr),mesg_in.hdr.id, mesg_in.hdr.room,mesg_in.hdr.oper, mesg_in.text);
	}
	closesocket(s);
	WSACleanup();
	gets((char*)&wsa);
	return 0;
}

