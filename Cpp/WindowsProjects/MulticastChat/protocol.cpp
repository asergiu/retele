#pragma pack(1)
#include "protocol.h"
#include <winsock2.h>
#include <Ws2ipdef.h>
#include <windows.h>
#include <iostream>
#include <map>
#include <string>
using namespace std;

namespace ChatProtocol
{
	System::String^ CharToString(const char *chr)
	{
		return gcnew System::String(chr);
	}

	enum Operations 
	{
		Join = 1,    //id, room, oper, msglen = 0
		Leave = 2,   //id, room, oper, msglen = 0
		Message = 3, //id, room, oper, msglen, message
		ListRequest = 4,    //id, oper, msglen = 0
		ListReply = 5 //id, msglen, message = rooms separated by 0, end with double 0
	};

	const int HeaderSize = 75;//8 + 32 + 32 + 1 + sizeof(unsigned short);

	const char address[] = "224.101.102.103";
	const short port = 7777;
	struct sockaddr_in addr;
	ip_mreq mc_req;
	int addrLen = sizeof(sockaddr_in);

	struct ChatMessage
	{
		char version[8];
		char room[32];
		char id[32];
		char operation;
		unsigned short msgLength;
		char msg[1];

		void init(int msgLen)
		{
			memset(this, 0, HeaderSize + msgLen);
			strcpy(version, "CHATv1");
		}

		void hton()
		{
			msgLength = htons(msgLength);
		}

		void ntoh()
		{
			msgLength = ntohs(msgLength);
		}
	};

	public ref class Events
	{
	public:
		MessageEvent^ Message;
		JoinEvent^ Join;
		LeaveEvent^ Leave;

		Events(MessageEvent^ me, JoinEvent^ je, LeaveEvent^ le)
		{
			Message = me;
			Join = je;
			Leave = le;
		}
	};

	map<string, bool> rooms;
	map<string, bool> roomList;
	int sock;
	CRITICAL_SECTION mutex;

	bool _destroy = false;

	void sendMsg(char *m, int size)
	{
		addr.sin_family = AF_INET;
		addr.sin_addr.s_addr = inet_addr(address);
		addr.sin_port = htons(port);
		sendto(sock, m, size, 0, (sockaddr*)&addr, sizeof(sockaddr_in));
	}

	int recvMsg(char *m)
	{
		int size = (1<<16) + HeaderSize;
		return recvfrom(sock, m, size, 0, (sockaddr*)&addr, &addrLen);
	}

	void listenThread(System::Object^ obj)
	{
		Events^ events = (Events^)obj;

		ChatMessage *m = (ChatMessage*)malloc(HeaderSize + (1 << 16));
		char *p;
		int n;


		while(!_destroy)
		{
			memset(m, 0, HeaderSize + (1<<16));
			if(recvMsg((char*)m) < 0)
				break;
			m->ntoh();

			if(strcmp(m->version, "CHATv1") != 0)
			{
				cout << strlen(m->version);
				cout << "++> different version: " << m->version << endl;
				//continue;
			}

			//printf("Op: %d %d\n", m->operation, m->msgLength);
			m->msg[m->msgLength] = 0;

			EnterCriticalSection(&mutex);

			switch(m->operation)
			{
			case Join:
				printf("=> Join: %s[%s]\n", m->id, m->room);
				if(rooms.find(string(m->room)) != rooms.end())
					events->Join(CharToString(m->room), CharToString(m->id));
				break;
			case Leave:
				printf("=> Leave: %s[%s]\n", m->id, m->room);
				if(rooms.find(string(m->room)) != rooms.end())
					events->Leave(CharToString(m->room), CharToString(m->id));
				break;
			case Message:
				printf("=> Message: %s[%s]: %s\n", m->id, m->room, m->msg);
				if(rooms.find(string(m->room)) != rooms.end())
					events->Message(CharToString(m->msg), CharToString(m->room), CharToString(m->id));
				break;
			case ListRequest:
				printf("=> List rooms request: %s\n", m->id);
				m->init(1<<16);

				p = m->msg;
				for(map<string, bool>::iterator it = rooms.begin(); it != rooms.end(); ++ it)
				{
					memcpy(p, (*it).first.c_str(), (*it).first.length());
					p += (*it).first.length();
					*p = 0;
					++ p;
				}
				if(rooms.size() == 0)
					*p = 0, ++ p;
				*p = 0;
				++ p;

				m->msgLength = p - m->msg;

				m->operation = ListReply;
				m->hton();
				sendMsg((char*)m, HeaderSize + p - m->msg);
				printf("<= List rooms reply\n");
				break;
			case ListReply:
				printf("=> List rooms reply\n");
				p = m->msg;

				while(*p)
				{
					roomList[string(p)] = true;
					p += strlen(p) + 1;
				}
				break;
			default:
				cout << "++> Invalid operation number " << m->operation << endl;
				break;
			}

			LeaveCriticalSection(&mutex);
		}
	}

	void initialize(MessageEvent^ msg, JoinEvent^ join, LeaveEvent^ leave)
	{
		InitializeCriticalSection(&mutex);
		WSADATA wsaData;
		WSAStartup(MAKEWORD(2,2), &wsaData);

		sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if(sock == INVALID_SOCKET)
			printf("Socket error\n");

		int flag_on = 1;
		if(setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char*)&flag_on, sizeof(flag_on)) < 0)
			printf("Reuse addr error\n");
		
		memset(&addr, 0, addrLen);
		addr.sin_family = AF_INET;
		addr.sin_addr.s_addr = INADDR_ANY;
		addr.sin_port = htons(port);
		if(bind(sock, (sockaddr*)&addr, addrLen) < 0)
			printf("Bind error\n");
		
		mc_req.imr_multiaddr.s_addr = inet_addr(address);
		mc_req.imr_interface.s_addr = htonl(INADDR_ANY);
		if ((setsockopt(sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char*) &mc_req, sizeof(mc_req))) < 0)
			printf("Add membership error\n");

		char mc_ttl = 2;
		if (setsockopt(sock, IPPROTO_IP, IP_MULTICAST_TTL, (char*) &mc_ttl, sizeof(mc_ttl)) < 0)
			printf("Set TTL error\n");

		System::Threading::Thread^ thread = gcnew System::Threading::Thread(gcnew System::Threading::ParameterizedThreadStart(&listenThread));
		thread->Start(gcnew Events(msg, join, leave));
	}

	void joinRoom(const char* room, const char *id)
	{
		EnterCriticalSection(&mutex);
		if(rooms.find(string(room)) != rooms.end())
		{
			LeaveCriticalSection(&mutex);
			return;
		}

			ChatMessage *m = (ChatMessage*)malloc(HeaderSize);
			m->init(0);
			memcpy(m->room, room, min(31, strlen(room)));
			memcpy(m->id, id, min(32, strlen(id)));
			m->operation = Join;

			rooms[string(m->room)] = true;
		LeaveCriticalSection(&mutex);

		sendMsg((char*)m, HeaderSize);
		printf("<= Join: %s[%s]\n", m->id, m->room);
		free(m);
	}

	void leaveRoom(const char* room, const char *id)
	{
		EnterCriticalSection(&mutex);
		if(rooms.find(string(room)) == rooms.end())
		{
			LeaveCriticalSection(&mutex);
			return;
		}

			ChatMessage *m = (ChatMessage*)malloc(HeaderSize);
			m->init(0);
			memcpy(m->room, room, min(31, strlen(room)));
			memcpy(m->id, id, min(32, strlen(id)));
			m->operation = Leave;

			rooms.erase(rooms.find(string(room)));
		LeaveCriticalSection(&mutex);


		sendMsg((char*)m, HeaderSize);
		printf("<= Leave: %s[%s]\n", id, room);
		free(m);
	}

	void sendMessage(const char* room, const char* id, const char *msg)
	{
		EnterCriticalSection(&mutex);
		if(rooms.find(string(room)) == rooms.end())
		{
			LeaveCriticalSection(&mutex);
			return;
		}
		LeaveCriticalSection(&mutex);

		printf("<= Message: %s[%s]: %s\n", id, room, msg);

		int len = strlen(msg);
		ChatMessage *m = (ChatMessage*)malloc(HeaderSize + len + 1);
		m->init(len + 1);
		memcpy(m->room, room, min(31, strlen(room)));
		memcpy(m->id, id, min(31, strlen(id)));
		memcpy(m->msg, msg, len);
		m->operation = Message;
		m->msgLength = len;
		m->hton();

		sendMsg((char*)m, HeaderSize + len + 1);
	}

	array<System::String^>^ listRooms(const char* id)
	{
		EnterCriticalSection(&mutex);
			roomList.clear();
		LeaveCriticalSection(&mutex);

		//send request message
		ChatMessage *m = (ChatMessage*)malloc(HeaderSize);
		m->init(0);
		memcpy(m->id, id, min(31, strlen(id)));
		m->operation = ListRequest;
		m->hton();

		sendMsg((char*)m, HeaderSize);
		printf("<= List rooms request: %s\n", id);

		//wait for replies
		System::Threading::Thread::Sleep(1000);

		//fill array
		EnterCriticalSection(&mutex);
			array<System::String^>^ rooms = gcnew array<System::String^>(roomList.size());
			int i = 0;
			for(map<string, bool>::iterator it = roomList.begin(); it != roomList.end() && i < rooms->Length; ++it)
				rooms[i ++] = CharToString((*it).first.c_str());
		LeaveCriticalSection(&mutex);
		return rooms;
	}

	void destroy(const char *name)
	{
		char temp[100];
		while(rooms.size())
		{
			strcpy(temp, rooms.begin()->first.c_str());
			leaveRoom(temp , name);
		}

		//free stuff
		if ((setsockopt(sock, IPPROTO_IP, IP_DROP_MEMBERSHIP, (char*) &mc_req, sizeof(mc_req))) < 0) 
			printf("DROP membership error\n");

		closesocket(sock);
		//set to exit thread
		_destroy = true;
		//wait for thread to finish
		System::Threading::Thread::Sleep(100);
		//clean rest
		WSACleanup();		
		DeleteCriticalSection(&mutex);
	}
}