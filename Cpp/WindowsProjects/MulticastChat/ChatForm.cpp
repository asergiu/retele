#include "ChatForm.h"
#include <memory.h>
#include "protocol.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

namespace MulticastChat
{
	void StringToChar(String ^in, char* out, int len)
	{
		memset(out, 0, len);
		for(int i=0; i < len - 1 && i < in->Length; ++i)
			out[i] = in[i];
	}

	void ChatForm::addRoom(System::String^ room)
	{
		for(int i=0; i < rooms->TabPages->Count; ++ i)
			if(rooms->TabPages[i]->Text == room)
			{
				rooms->SelectedIndex = i;
				return;
			}
		
		TabPage ^page = gcnew TextTabPage(room);
		rooms->TabPages->Add(page);
		rooms->SelectedTab = page;

		char chroom[32];
		StringToChar(rooms->SelectedTab->Text, chroom, 32);
		char id[32];
		StringToChar(name->Text->Trim(), id, 32);
		ChatProtocol::joinRoom(chroom, id);
	}

	void ChatForm::removeRoom(TabPage^ toRemove)
	{
		String ^room = toRemove->Text;
		rooms->TabPages->Remove(toRemove);

		char chroom[32];
		StringToChar(room, chroom, 32);
		char id[32];
		StringToChar(name->Text->Trim(), id, 32);
		ChatProtocol::leaveRoom(chroom, id);
	}

	void ChatForm::sendMessage(String^ message, String ^Room, String ^Name)
	{
		char *msg = new char[message->Length + 1];
		StringToChar(message, msg, message->Length + 1);
		char room[32];
		StringToChar(Room, room, 32);
		char id[32];
		StringToChar(Name, id, 32);
		ChatProtocol::sendMessage(room, id, msg);
	}

	void ChatForm::listRooms()
	{
		char id[32];
		StringToChar(name->Text->Trim(), id, 32);
		array<String^>^ res = ChatProtocol::listRooms(id);
		String^ rooms = "Rooms:";
		for(int i=0;i<res->Length;++i)
			rooms += "\n" + res[i];
		MessageBox::Show(rooms, "Rooms list");
	}


}