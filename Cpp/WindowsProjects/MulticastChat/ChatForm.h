#pragma once
#include "MyTabPage.h"
#include "protocol.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;


namespace MulticastChat 
{
	void StringToChar(String ^in, char* out, int len);

	public ref class ChatForm : public System::Windows::Forms::Form
	{

	private:
		void addRoom(String^ room);

		void removeRoom(TabPage^ toRemove);

		void ChatForm::sendMessage(String^ message, String ^Room, String ^Name);

		void recvMsgSync(String^ msg, String^ room, String^ id)
		{
			for(int i=0; i < rooms->TabCount; ++i)
				if(rooms->TabPages[i]->Text == room)
				{
					TextBox^ tb = ((TextTabPage^)rooms->TabPages[i])->conversation;
					tb->Text += id + ": " + msg + "\r\n";
					tb->Select(tb->Text->Length, 0);
					tb->ScrollToCaret();
					return;
				}
		}

		void recvMsg(String^ msg, String^ room, String^ id)
		{
			this->BeginInvoke(gcnew ChatProtocol::MessageEvent(this, &ChatForm::recvMsgSync), msg, room, id);
			
		}


		void recvJoinSync(String^ room, String ^ id)
		{
			for(int i=0; i < rooms->TabCount; ++i)
				if(rooms->TabPages[i]->Text == room)
				{					
					TextBox^ tb = ((TextTabPage^)rooms->TabPages[i])->conversation;
					tb->Text += id + " has joined the room\r\n";
					tb->Select(tb->Text->Length, 0);
					tb->ScrollToCaret();
					return;
				}
		}

		void recvJoin(String^ room, String ^ id)
		{
			this->BeginInvoke(gcnew ChatProtocol::JoinEvent(this, &ChatForm::recvJoinSync), room, id);
		}

		void recvLeaveSync(String^ room, String ^ id)
		{
			for(int i=0; i < rooms->TabCount; ++i)
				if(rooms->TabPages[i]->Text == room)
				{
					TextBox^ tb = ((TextTabPage^)rooms->TabPages[i])->conversation;
					tb->Text += id + " has left the room\r\n";
					tb->Select(tb->Text->Length, 0);
					tb->ScrollToCaret();
					return;
				}
		}

		void recvLeave(String^ room, String ^ id)
		{
			this->BeginInvoke(gcnew ChatProtocol::LeaveEvent(this, &ChatForm::recvLeaveSync), room, id);
		}

		void listRooms();

	public:
		ChatForm(void)
		{
			InitializeComponent();
			ChatProtocol::initialize(gcnew ChatProtocol::MessageEvent(this, &ChatForm::recvMsg),
				gcnew ChatProtocol::JoinEvent(this, &ChatForm::recvJoin),
				gcnew ChatProtocol::LeaveEvent(this, &ChatForm::recvLeave));
			this->Text = "Multicast Chat v1";
		}

	protected:
		~ChatForm()
		{
			if (components)
			{
				delete components;
			}
			char nm[32];
			StringToChar(name->Text->Trim(), nm, 31);
			ChatProtocol::destroy(nm);
		}
	private: System::Windows::Forms::TabControl^  rooms;
	private: System::Windows::Forms::TextBox^  message;
	private: System::Windows::Forms::TextBox^  room;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::TextBox^  name;
	private: System::Windows::Forms::Button^  btnListRooms;
	private: System::Windows::Forms::Button^  btnSend;
	private: System::Windows::Forms::Button^  btnJoin;



	private:
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->rooms = (gcnew System::Windows::Forms::TabControl());
			this->message = (gcnew System::Windows::Forms::TextBox());
			this->room = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->name = (gcnew System::Windows::Forms::TextBox());
			this->btnListRooms = (gcnew System::Windows::Forms::Button());
			this->btnSend = (gcnew System::Windows::Forms::Button());
			this->btnJoin = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// rooms
			// 
			this->rooms->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
				| System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->rooms->Location = System::Drawing::Point(3, 1);
			this->rooms->Margin = System::Windows::Forms::Padding(0);
			this->rooms->Name = L"rooms";
			this->rooms->SelectedIndex = 0;
			this->rooms->Size = System::Drawing::Size(671, 361);
			this->rooms->TabIndex = 0;
			this->rooms->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &ChatForm::rooms_MouseClick);
			// 
			// message
			// 
			this->message->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->message->Location = System::Drawing::Point(60, 393);
			this->message->MaxLength = 62000;
			this->message->Multiline = true;
			this->message->Name = L"message";
			this->message->Size = System::Drawing::Size(530, 20);
			this->message->TabIndex = 5;
			this->message->KeyUp += gcnew System::Windows::Forms::KeyEventHandler(this, &ChatForm::message_KeyUp);
			// 
			// room
			// 
			this->room->AcceptsReturn = true;
			this->room->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->room->Location = System::Drawing::Point(60, 367);
			this->room->MaxLength = 31;
			this->room->Multiline = true;
			this->room->Name = L"room";
			this->room->Size = System::Drawing::Size(179, 20);
			this->room->TabIndex = 1;
			this->room->KeyUp += gcnew System::Windows::Forms::KeyEventHandler(this, &ChatForm::room_KeyUp);
			// 
			// label1
			// 
			this->label1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(0, 370);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(55, 13);
			this->label1->TabIndex = 3;
			this->label1->Text = L"Join room:";
			// 
			// label2
			// 
			this->label2->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(1, 396);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(53, 13);
			this->label2->TabIndex = 4;
			this->label2->Text = L"Message:";
			// 
			// label3
			// 
			this->label3->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(465, 370);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(38, 13);
			this->label3->TabIndex = 5;
			this->label3->Text = L"Name:";
			// 
			// name
			// 
			this->name->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->name->Location = System::Drawing::Point(509, 367);
			this->name->MaxLength = 31;
			this->name->Name = L"name";
			this->name->Size = System::Drawing::Size(162, 20);
			this->name->TabIndex = 4;
			this->name->Text = L"dadi";
			// 
			// btnListRooms
			// 
			this->btnListRooms->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->btnListRooms->Location = System::Drawing::Point(317, 365);
			this->btnListRooms->Name = L"btnListRooms";
			this->btnListRooms->Size = System::Drawing::Size(66, 23);
			this->btnListRooms->TabIndex = 3;
			this->btnListRooms->Text = L"List rooms";
			this->btnListRooms->UseVisualStyleBackColor = true;
			this->btnListRooms->Click += gcnew System::EventHandler(this, &ChatForm::btnListRooms_Click);
			// 
			// btnSend
			// 
			this->btnSend->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->btnSend->Location = System::Drawing::Point(596, 391);
			this->btnSend->Name = L"btnSend";
			this->btnSend->Size = System::Drawing::Size(75, 23);
			this->btnSend->TabIndex = 6;
			this->btnSend->Text = L"Send";
			this->btnSend->UseVisualStyleBackColor = true;
			this->btnSend->Click += gcnew System::EventHandler(this, &ChatForm::btnSend_Click);
			// 
			// btnJoin
			// 
			this->btnJoin->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->btnJoin->Location = System::Drawing::Point(245, 365);
			this->btnJoin->Name = L"btnJoin";
			this->btnJoin->Size = System::Drawing::Size(66, 23);
			this->btnJoin->TabIndex = 2;
			this->btnJoin->Text = L"Join";
			this->btnJoin->UseVisualStyleBackColor = true;
			this->btnJoin->Click += gcnew System::EventHandler(this, &ChatForm::btnJoin_Click);
			// 
			// ChatForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(675, 425);
			this->Controls->Add(this->btnJoin);
			this->Controls->Add(this->btnSend);
			this->Controls->Add(this->btnListRooms);
			this->Controls->Add(this->name);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->room);
			this->Controls->Add(this->message);
			this->Controls->Add(this->rooms);
			this->Name = L"ChatForm";
			this->Text = L"ChatForm";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: 
		void room_KeyUp(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e) 
		{
			if(e->KeyCode == Keys::Enter)
			{
				if(room->Text->Trim() != "")
					addRoom(room->Text->Trim());
				room->Text = "";
			}
		}

		void rooms_MouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) 
		{
			if(e->Button == System::Windows::Forms::MouseButtons::Middle || e->Button == System::Windows::Forms::MouseButtons::Right)
			{
				for(int i=0; i < rooms->TabPages->Count; ++ i)
					if(rooms->GetTabRect(i).Contains(e->Location))
					{
						removeRoom(rooms->TabPages[i]);
						return;
					}
			}
		}

		void message_KeyUp(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e) 
		{
			if(e->KeyCode == Keys::Enter)
			{
				if(message->Text->Trim() != "")
					if(rooms->TabCount)
						sendMessage(message->Text->Trim(), rooms->SelectedTab->Text, name->Text->Trim());
				message->Text = "";
			}
		}
		
		System::Void btnSend_Click(System::Object^  sender, System::EventArgs^  e) 
		{
			if(message->Text->Trim() != "")
				if(rooms->TabCount)
					sendMessage(message->Text->Trim(), rooms->SelectedTab->Text, name->Text->Trim());
			message->Text = "";
		}
		
		System::Void btnJoin_Click(System::Object^  sender, System::EventArgs^  e) 
		{
			if(room->Text->Trim() != "")
				addRoom(room->Text->Trim());
			room->Text = "";
		}
		
		System::Void btnListRooms_Click(System::Object^  sender, System::EventArgs^  e) 
		{
			listRooms();
		}
	};
}
