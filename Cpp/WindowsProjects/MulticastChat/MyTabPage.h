#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;


namespace MulticastChat 
{
	public ref class TextTabPage : public TabPage
	{
	public:
		TextBox ^conversation;

		TextTabPage(String ^text) : TabPage(text)
		{
			conversation = gcnew TextBox();
			conversation->Text = "";
			conversation->Parent = this;
			conversation->AcceptsReturn = true;
			conversation->AcceptsTab = true;
			conversation->ReadOnly = true;
			conversation->Dock = DockStyle::Fill;
			conversation->Multiline = true;
			conversation->ScrollBars = System::Windows::Forms::ScrollBars::Vertical;
		}
	};
}