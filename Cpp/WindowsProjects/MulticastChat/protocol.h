#pragma once

namespace ChatProtocol
{	
	System::String^ CharToString(const char *chr);

	public delegate void MessageEvent(System::String^ msg, System::String^ room, System::String^ id);
	public delegate void JoinEvent(System::String^ room, System::String^ id);
	public delegate void LeaveEvent(System::String^ room, System::String^ id);

	void initialize(MessageEvent^ msg, JoinEvent^ join, LeaveEvent^ leave);

	void joinRoom(const char* room, const char* id);

	void leaveRoom(const char* room, const char* id);

	void sendMessage(const char* room, const char *id, const char *msg);

	array<System::String^>^ listRooms(const char* id);

	void destroy(const char *name);
}
