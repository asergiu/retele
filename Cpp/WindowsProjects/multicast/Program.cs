﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.Diagnostics;

namespace multicast
{
    class Program
    {
        private static String nickname = "guest";
        private static List<String> rooms = new List<String>();
        //private static List<KeyValuePair<string, bool>> rooms = new List<KeyValuePair<string, bool>>();
        private static Socket socket;
        private static IPEndPoint ipep;
        private static ManageCommand manageCommand = new ManageCommand();
        private static MultiMessage multiMessage = new MultiMessage();

        static void Main(string[] args)
        {
            StringBuilder welcome = new StringBuilder();
            welcome.AppendLine("--------------------------------------------------------------------");
            welcome.AppendLine("UDP multicast chat application");
            welcome.AppendLine("--------------------------------------------------------------------");
            welcome.AppendLine("LIST – returns the list of rooms");
            welcome.AppendLine("JOIN <room name> - subscribes to a room (send corresponding message)");
            welcome.AppendLine("LEAVE <room name> - leave room (send corresponding message)");
            welcome.AppendLine("MSG <room name> <msg>  - send message to room");
            welcome.AppendLine("NICK <nickname> - sets the ID for further correspondence.");
            welcome.AppendLine("QUIT – unsubscribe from all rooms, close sockets and exit.");
            welcome.AppendLine("\nYou can tape a command");
            welcome.AppendLine("--------------------------------------------------------------------");
            Console.Write(welcome.ToString());

            // start the multicast
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            ipep = new IPEndPoint(IPAddress.Any, new Config().Port);
            socket.Bind(ipep);
            MulticastOption mCastOpt = new MulticastOption(IPAddress.Parse(new Config().MCast), IPAddress.Any);
            socket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.AddMembership, mCastOpt);

            // start the the to check messages in real time
            Thread checkMessages;
            checkMessages = new Thread(new ThreadStart(CheckMessages));
            checkMessages.Start();

            // start the thread of the menu
            Thread command;
            command = new Thread(new ThreadStart(Command));
            command.Start(); 
           
        }

        /// <summary>
        /// manage the different command
        /// </summary>
        public static void Command()
        {
            while (Thread.CurrentThread.IsAlive)
            {
                Thread.Sleep(1);

                String inputText = Console.ReadLine();

                if (manageCommand.IsCommand(inputText, "LIST", 0))
                    ListOfRooms();
                else 
                    if (manageCommand.IsCommandMsg(inputText))
                    ManageMsg(inputText);
                else if (manageCommand.IsCommand(inputText, "QUIT", 0))
                    Quit();
                else if (manageCommand.IsCommand(inputText, "JOIN", 1))
                    JoinRooms(inputText);
                else if (manageCommand.IsCommand(inputText, "LEAVE", 1))
                    LeaveRoom(inputText);
                else if (manageCommand.IsCommand(inputText, "NICK", 1))
                    ManageNickname(inputText);
                else
                    Console.WriteLine("unknown command");
            }
        }

        /// <summary>
        /// check the messages
        /// </summary>
        public static void CheckMessages()
        {
            while (Thread.CurrentThread.IsAlive)
            {
                Thread.Sleep(1);
                MultiReceive(socket);
            }
        }

        /// <summary>
        /// manage the subscription in rooms
        /// </summary>
        /// <param name="inputText"></param>
        public static void JoinRooms(String inputText)
        {
            String joinRoom = manageCommand.GetParameter(inputText, 1);
            if (rooms.Count == 0)
            {
                Console.WriteLine("The room doesn't exist. The room has been created");
                rooms.Add(joinRoom);
                MultiSend(socket, joinRoom, nickname, "1", String.Format("{0} joins the room {1}", nickname, joinRoom));
            }
            else if (ListFind(rooms, joinRoom))
            {
                Console.WriteLine(String.Format("You have already subscribe to the room {0}", joinRoom));
            }
            else
            {
                bool roomExist = false;
                foreach (string room in rooms)
                {
                    if (room.Equals(joinRoom))
                    {
                        MultiSend(socket, joinRoom, nickname, "1", String.Format("{0} joins the room {1}", nickname, joinRoom));
                        roomExist = true;
                        break;
                    }
                }
                if (!roomExist)
                {
                    Console.WriteLine("The room doesn't exist. The room has been created");
                    rooms.Add(joinRoom);
                    MultiSend(socket, joinRoom, nickname, "1", String.Format("{0} joins the room {1}", nickname, joinRoom));
                }
            }
        }

        /// <summary>
        /// manage the list of rooms
        /// </summary>
        public static void ListOfRooms()
        {
            if (rooms.Count == 0)
                Console.WriteLine("You are registered to any room");
            else
            {
                Console.WriteLine("You subscribed to those rooms :");
                foreach (String room in rooms)
                    Console.WriteLine(room.ToString());
            }
            Console.WriteLine("\nOther rooms in the network :");
            MultiSend(socket, "", nickname, "4", "");
        }

        /// <summary>
        /// management when the user leave rooms 
        /// </summary>
        /// <param name="inputText"></param>
        public static void LeaveRoom(String inputText)
        {
            if (rooms.Count == 0)
                Console.WriteLine("There is no room to leave");
            else 
            {
                String roomToDelete = null;
                foreach (String room in rooms)
                {
                    if (manageCommand.GetParameter(inputText, 1).Equals(room))
                    {
                        roomToDelete = room;
                        break;
                    }
                }
                if (roomToDelete != null)
                {
                    rooms.Remove(roomToDelete);
                    MultiSend(socket, roomToDelete, nickname, "2", String.Format("{0} leaves the room {1}",nickname, roomToDelete));
                }
                else
                {
                    Console.WriteLine("The room doesn't exist");
                }
            }
        }

        /// <summary>
        /// management of the message
        /// </summary>
        /// <param name="inputText"></param>
        public static void ManageMsg(String inputText)
        {
            String roomName = manageCommand.GetParameter(inputText, 1);
            String textMsg = manageCommand.GetTexteMessage(inputText);
            if (ListFind(rooms, roomName))
                MultiSend(socket, roomName, nickname, "3", textMsg);
            else
                Console.WriteLine(String.Format("You are not connected to the room {0} ", roomName));
        }

        /// <summary>
        /// save the new nickname
        /// </summary>
        /// <param name="inputText"></param>
        public static void ManageNickname(String inputText)
        {
            nickname = manageCommand.GetParameter(inputText, 1);
            Console.WriteLine(String.Format("Your nickname is now {0} ", nickname));
        }

        /// <summary>
        /// to quit the app
        /// </summary>
        public static void Quit()
        {
            rooms.Clear();
            socket.Close();
            Process.GetCurrentProcess().Kill();
        }

        /// <summary>
        /// send the message with the socket
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="roomName"></param>
        /// <param name="id"></param>
        /// <param name="oper"></param>
        /// <param name="message"></param>
        public static void MultiSend(Socket socket, String roomName, String id, String oper, String message)
        {
            socket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.MulticastTimeToLive, 1);
            IPEndPoint iep = new IPEndPoint(IPAddress.Parse(new Config().MCast), new Config().Port);

            Byte[] messageToSend = multiMessage.CreateMessage(roomName, id, oper, message);
            socket.SendTo(messageToSend, iep);
        }

        /// <summary>
        /// receive the message with the socket
        /// </summary>
        /// <param name="socket"></param>
        public static void MultiReceive(Socket socket)
		{
			Byte[] message = new byte[330];
            socket.Receive(message);

            string msgVersion = multiMessage.GetVersion(message);
            string msgOper = multiMessage.GetOper(message);
            string msgNickname = multiMessage.GetNickName(message);
            string msgRoom = multiMessage.GetRoomName(message);
            string msgTexte = multiMessage.GetMessage(message);

            bool displayMessage = true;

            if (String.IsNullOrEmpty(msgOper))
                displayMessage = false;

            if (msgOper.Equals("1"))
                if (!ListFind(rooms, msgRoom) && nickname.CompareTo(msgNickname) != 0)
                    Console.WriteLine(String.Format("{0} has created the room {1} ",msgNickname, msgRoom));

            if (!msgVersion.Equals(new Config().Release))
                displayMessage = false;

            if (msgOper.Equals("4"))
            {
                displayMessage = false;
                MultiSend(socket, "", nickname, "5", FormatListRooms());
            }

            if (msgOper.Equals("5"))
                displayMessage = false;

            if (msgOper.Equals("5") && nickname.CompareTo(msgNickname) != 0)
                displayListRooms(msgTexte);

            if (msgOper.Equals("3") && !ListFind(rooms, msgRoom))
                displayMessage = false;

            if (msgOper.Equals("1"))
                msgTexte = String.Format("{0} joins the room", msgNickname); ;

            if (displayMessage)
            {
                Console.WriteLine(String.Format("[{0}] in {1} : {2}", msgNickname, msgRoom, msgTexte));
                Console.WriteLine("--------------------------------------------------------------------");
            }
		}

        /// <summary>
        /// check if an element exist in a list
        /// </summary>
        /// <param name="list"></param>
        /// <param name="TheSearchCriteria"></param>
        /// <returns></returns>
        private static bool ListFind(List<String> list, String TheSearchCriteria)
        {
            String match = list.Find(
                delegate(String element) 
                {
                    return element.Equals(TheSearchCriteria); 
                });

            if (String.IsNullOrEmpty(match))
                return false;
            else
                return true;
        }

        /// <summary>
        /// format the list of the room to reply the list rooms request
        /// </summary>
        /// <returns></returns>
        private static String FormatListRooms()
        {
            StringBuilder roomList = new StringBuilder(); ;
            foreach(String room in rooms)
            {
                roomList.Append(String.Format("{0}NULL", room));
            }
            return roomList.Append("NULL").ToString();
        }

        /// <summary>
        /// display the reply list of rooms
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private static void displayListRooms(String sRoomList)
        {
            String[] arrayRoomList = sRoomList.Split(new string[] { "NULL" }, StringSplitOptions.None);
            List<String> listRoomList = arrayRoomList.ToList();
            listRoomList.RemoveAt(arrayRoomList.Count() -1);
            foreach (String room in arrayRoomList)
                Console.WriteLine(room);
        }
    }
}