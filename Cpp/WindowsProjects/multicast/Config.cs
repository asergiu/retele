﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace multicast
{
    class Config
    {
        private const String release = "CHATv1";
        private const String mcast = "224.0.0.1";
        private const int port = 7777;

        /// <summary>
        /// version of the chat
        /// </summary>
        public String Release
        {
            get { 
                return release; 
            }
        }

        /// <summary>
        /// multicast address
        /// </summary>
        public String MCast
        {
            get
            {
                return mcast;
            }
        }

        /// <summary>
        /// UDP port 7777
        /// </summary>
        public int Port
        {
            get
            {
                return port;
            }
        }

    }
}
