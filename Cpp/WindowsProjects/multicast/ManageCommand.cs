﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace multicast
{
    class ManageCommand
    {
        /// <summary>
        /// check if a command is valid or not
        /// </summary>
        /// <param name="inputCommand"></param>
        /// <param name="nameCommand"></param>
        /// <param name="numberParameter"></param>
        /// <returns></returns>
        public bool IsCommand(string inputCommand, string nameCommand, int numberParameter)
        {
            String[] command = inputCommand.Split(new char[] { ' ' });
            if (Regex.IsMatch(command[0], nameCommand, RegexOptions.IgnoreCase) && command.Length == numberParameter + 1)
                return true;
            else
                return false;
        }

        /// <summary>
        /// check if the command msg is valid
        /// </summary>
        /// <param name="inputCommand"></param>
        /// <returns></returns>
        public bool IsCommandMsg(string inputCommand)
        {
            String[] command = inputCommand.Split(new char[] { ' ' });
            if (Regex.IsMatch(command[0], "MSG", RegexOptions.IgnoreCase) && command.Length >= 3)
                return true;
            else
                return false;
        }

        /// <summary>
        /// get a parameter of the command
        /// </summary>
        /// <param name="inputCommand"></param>
        /// <param name="numberParameter"></param>
        /// <returns></returns>
        public String GetParameter(string inputCommand, int numberParameter)
        {
            String[] commandMsg = inputCommand.Split(new char[] { ' ' });
            return commandMsg[numberParameter];
        }

        /// <summary>
        /// get the texte of the message in the command "msg"
        /// </summary>
        /// <param name="inputCommand"></param>
        /// <returns></returns>
        public String GetTexteMessage(string inputCommand)
        {
            List<String> commandMsg = inputCommand.Split(new char[] { ' ' }).ToList();
            commandMsg.RemoveRange(0, 2);
            return String.Join(" ", commandMsg.ToArray());
        }
    }
}
