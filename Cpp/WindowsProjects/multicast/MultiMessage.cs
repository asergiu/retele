﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace multicast
{
    class MultiMessage
    {
        /// <summary>
        /// create the message with a specific form
        /// </summary>
        /// <param name="RoomName"></param>
        /// <param name="Id"></param>
        /// <param name="Oper"></param>
        /// <param name="Message"></param>
        /// <returns></returns>
        public Byte[] CreateMessage(string RoomName, string Id, string Oper, string Message)
        {
            Byte[] bufferVersion = addZeroBefore(Encoding.ASCII.GetBytes(new Config().Release), 8);
            Byte[] bufferRoomName = addZeroBefore(Encoding.ASCII.GetBytes(RoomName), 32);
            Byte[] bufferId = addZeroBefore(Encoding.ASCII.GetBytes(Id), 32);
            Byte[] bufferOper = addZeroBefore(Encoding.ASCII.GetBytes(Oper), 1);
            Byte[] bufferMessage = Encoding.ASCII.GetBytes(Message);
            Byte[] bufferLength = addZeroBefore(Encoding.ASCII.GetBytes(bufferMessage.Length.ToString()), 2);

            Byte[] bufferReturn = new Byte[330];
            bufferVersion.CopyTo(bufferReturn, 0);
            bufferRoomName.CopyTo(bufferReturn, 8);
            bufferId.CopyTo(bufferReturn, 40);
            bufferOper.CopyTo(bufferReturn, 72);
            bufferLength.CopyTo(bufferReturn, 73);
            bufferMessage.CopyTo(bufferReturn, 75);

            return bufferReturn;
        }

        /// <summary>
        /// add somes zeros to complete the string with a determined value
        /// </summary>
        /// <param name="tabBytes"></param>
        /// <param name="stringLenght"></param>
        /// <returns></returns>
        public static Byte[] addZeroBefore(Byte[] tabBytes, int stringLenght)
        {
            Byte[] realLong = new Byte[stringLenght];
            tabBytes.CopyTo(realLong, stringLenght - tabBytes.Length);
            return realLong;
        }

        /// <summary>
        /// get an element of the message
        /// </summary>
        /// <param name="message"></param>
        /// <param name="beginIndex"></param>
        /// <param name="lenght"></param>
        /// <returns></returns>
        public static String GetElement(Byte[] message, int beginIndex, int lenght)
        {
            String element = Encoding.ASCII.GetString(message, beginIndex, lenght);
            element = element.Replace("\0", "");
            return element;
        }

        /// <summary>
        /// get the field version in the message
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public String GetVersion(Byte[] message)
        {
            return GetElement(message, 0, 8);
        }

        /// <summary>
        /// get the field roomName in the message
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public String GetRoomName(Byte[] message)
        {
            return GetElement(message, 8, 32);
        }

        /// <summary>
        /// get the field nickname in the message
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public String GetNickName(Byte[] message)
        {
            return GetElement(message, 40, 32);
        }

        /// <summary>
        /// get the field operation in the message
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public String GetOper(Byte[] message)
        {
            return GetElement(message, 72, 1);
        }

        /// <summary>
        /// get the field length in the message
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public String GetLenght(Byte[] message)
        {
            return GetElement(message, 73, 2);
        }

        /// <summary>
        /// get the field message in the message
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public String GetMessage(Byte[] message)
        {
            return GetElement(message, 75, 255);
        }

    }
}
