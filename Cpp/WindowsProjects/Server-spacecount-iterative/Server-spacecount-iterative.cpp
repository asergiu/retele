#include <stdio.h>
//#define _WINSOCK_DEPRECATED_NO_WARNINGS

#ifndef WIN32
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#define closesocket close
typedef int SOCKET;

#else
#include <WinSock.h>
#pragma comment(lib,"Ws2_32.lib")
#endif
#include <iostream>

int main() {
	SOCKET s;
	struct sockaddr_in server, client;
	int c, l, err;
	unsigned int  counter = 0;
	char string[100];
#ifdef WIN32
	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) < 0) {
		std::cout << "Error initializong the WSA" << std::endl;
		return -1;
	}
#endif
	s = socket(AF_INET, SOCK_STREAM, 0);
	if (s < 0) {
		std::cout << "Eroare la crearea socketului server \n";
		return -1;
	}
	memset(&server, 0, sizeof(server));
	server.sin_port = htons(1234);
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;

	if (bind(s, (struct sockaddr*)&server, sizeof(server)) < 0) {
		perror("BindError: \n");
		return 1;
	}
	listen(s, 5);
	l = sizeof(client);
	memset(&client, 0, sizeof(client));

	while (1) {	
		std::cout << "Listening for incoming connections \n";
		c = accept(s, (struct sockaddr*)&client, &l);
		err = errno;
#ifdef WIN32
		err = WSAGetLastError();
#endif
		if (c < 0) {
			std::cout << "Accept error: " << err;
			continue;
		}
		printf("Incomming connected client from: %s : %d\n",inet_ntoa(client.sin_addr), ntohs(client.sin_port));
		//serving the connected client
		counter = 0;
		int len = 0;
		int res = recv(c, (char*)&len, sizeof(int), 0);
		len = ntohs(len);
		std::cout << len << "\n";
		res = recv(c, string, len, 0);
		//std::cout << "String is: " << string;
		for (int i = 0; i < len; i++) {
			std::cout << string[i];
			if (string[i] == ' ')
				counter += 1;
		}
		std::cout << counter << "; " << sizeof(counter) << "\n";
		counter = htonl(counter);
		res = send(c, (const char *)&counter, sizeof(counter), 0);
		if (res != sizeof(counter)) std::cout << "Error sending result!\n";
		closesocket(c);
	}
#ifdef WIN32
	WSACleanup();
#endif
}
