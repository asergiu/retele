#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
 
int main(int argc, char **argv) {
  int c;
  struct sockaddr_in server;
  uint32_t a, b, suma;
  
  if (argc < 2 ) {
      printf("%s <server_ip>\n",argv[0]);
      return -1;
  }
  
  c = socket(AF_INET, SOCK_STREAM, 0);
  if (c < 0) {
    perror("Error creating socket: ");
    return -2;
  }
  
  memset(&server, 0, sizeof(server));
  server.sin_port = htons(1234);
  server.sin_family = AF_INET;
  server.sin_addr.s_addr = inet_addr(argv[1]);
  
  if (connect(c, (struct sockaddr *) &server, sizeof(server)) < 0) {
    perror("Error connecting to the server: ");
    return -3;
  }
  

  printf("a = ");
  scanf("%u", &a);
  printf("b = ");
  scanf("%u", &b);
  a = htons(a);
  b = htons(b);
  int res = send(c, &a, sizeof(a), 0);
  res = send(c, &b, sizeof(b), 0);
  res = recv(c, &suma, sizeof(suma), 0);
  suma = ntohs(suma);
  printf("Sum: %u\n", suma);
  close(c);
  return 0;
}
