#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
 
int main() {
  int s;
  struct sockaddr_in server, client;
  int sock_client;
  unsigned int len;
  char ip_as_string[16];

  s = socket(AF_INET, SOCK_STREAM, 0);
  if (s < 0) {
    printf("Eroare la crearea socketului server\n");
    return -1;
  }

  
  memset(&server, 0, sizeof(server));
  server.sin_port = htons(1234);
  server.sin_family = AF_INET;
  server.sin_addr.s_addr = INADDR_ANY;

  
  if (bind(s, (struct sockaddr *) &server, sizeof(server)) < 0) {
    perror("Error binding address to socket");
    return -2;
  }
 
  if (listen(s, 5) < 0 ){
      perror("Error setting up the backlog queue: ");
      return -3;
  }

  len = sizeof(client);
  memset(&client, 0, sizeof(client));
  
  while (1) {
    uint16_t a, b, suma;
    sock_client = accept(s, (struct sockaddr *) &client, (socklen_t*) &len);
    
    //printf("New incomming connection from: %s\n",inet_ntoa(client.sin_addr));
    if ( !inet_ntop(AF_INET,&(client.sin_addr),ip_as_string,sizeof(ip_as_string)) )
      printf("Error inet_ntop\n");
    printf("New incomming connection from: %s\n",ip_as_string);
    
    
    //serve the new client - Cannot serve other clients while serving this one
    int res = recv(sock_client, &a, sizeof(a), 0);
    res = recv(sock_client, &b, sizeof(b), 0);
    a = ntohs(a);
    b = ntohs(b);
    printf("Received %u\n",a);
    printf("Received %u\n",b);
    suma = a + b;
    suma = htons(suma);
    send(sock_client, &suma, sizeof(suma), 0);
    close(sock_client);
    printf("Connection to the client %s closed\n",inet_ntoa(client.sin_addr));
    // END serving the client - we return to waiting other clients
  }
}