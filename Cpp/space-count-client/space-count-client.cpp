#include <stdio.h>
#include <arpa/inet.h>

#ifndef WIN32
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#define closesocket close
typedef int SOCKET;
#endif

int main(){
	int c;
	struct sockaddr_in server;
	char string[100];
	unsigned int counter=0;
	int res;
	c= socket(AF_INET, SOCK_STREAM, 0);
	if (c < 0){
		printf("Eroare la crearea socketului\n");
		return 1;
	}
	memset(&server, 0 , sizeof(server));
	server.sin_port = htons(1234);
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = inet_addr("192.168.0.16");
	if(connect(c,(struct sockaddr*)&server, sizeof(server)) < 0) {
		printf("eroare la conectarea la server");
		return 1;
	}
	printf("String is: ");
	scanf("%[^\n]", string);

	
	printf("%s", string);
	
	int len = strlen(string)+1;//+1 to add '\0';
	printf("\n%d \n", len);

	len = htons(len);

	res = send(c, &len, sizeof(len), 0); 
	res = send(c, string, ntohs(len), 0);

	res = recv(c, &counter, sizeof(counter),0);

	printf("%d, %ld\n", counter, sizeof(counter));

	counter = ntohl(counter);
	printf("Number of spaces in the given string is %d\n", counter);
	close(c);
}
