#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
 

 
void handle_client(int sock_client, struct sockaddr_in* client){

    uint32_t a, b, suma;
    
    
    printf("New incomming connection from: %s\n",inet_ntoa(client->sin_addr));
    
    recv(sock_client, &a, sizeof(a), 0);
    recv(sock_client, &b, sizeof(b), 0);
    a = ntohl(a);
    b = ntohl(b);
    printf("Received: %u\n",a);
    printf("Received: %u\n",b);
    suma = a + b;
    suma = htonl(suma);
    send(sock_client, &suma, sizeof(suma), 0);
    
    //close client socket - we've done here
    close(sock_client); 
    printf("Connection to the client %s closed\n",inet_ntoa(client->sin_addr));
    
    return;
    /* or exit(0) */
}
 
int main() {
  struct sockaddr_in server, client;
  int sock_client, len;
  int s;
  
  s = socket(AF_INET, SOCK_STREAM, 0);
  if (s < 0) {
    printf("Eroare la crearea socketului server\n");
    return -1;
  }
  
  memset(&server, 0, sizeof(server));
  server.sin_port = htons(1234);
  server.sin_family = AF_INET;
  server.sin_addr.s_addr = INADDR_ANY;
  
  if (bind(s, (struct sockaddr *) &server, sizeof(server)) < 0) {
    perror("Error binding address to socket");
    return -2;
  }
 
  if (listen(s, 5) < 0 ){
      perror("Error setting up the backlog queue: ");
      return -3;
  }
  
  len = sizeof(client);
  memset(&client, 0, sizeof(client));
  
  while (1) {
    printf("Server accepting clients on port: 1234\n");
    sock_client = accept(s, (struct sockaddr *) &client, (socklen_t*)&len);
    /* handle sock_client < 0 */

    int pid = fork();
    if (pid < 0 ){
        perror("Error creating new process: ");
        close(sock_client);
        /*handle the situation in some way */
    }
    
    if (pid == 0 ){  //child process 
        //close Rendez-vous socket - it is not used here
        close(s);
        
        handle_client(sock_client, &client);
        
        //do not forget to exit() - otherwise evil things will happen with your processes
        exit(0);
    }
    // we are in the parent (server) process here
    
  }
}